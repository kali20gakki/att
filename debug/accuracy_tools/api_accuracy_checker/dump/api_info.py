# 定义API INFO，保存基本信息，用于后续结构体的落盘，注意考虑random场景及真实数据场景
import os
import inspect
import torch
from api_accuracy_checker.common.config import msCheckerConfig
from api_accuracy_checker.common.utils import print_error_log, write_pt, create_directory, DumpException
from ptdbg_ascend.src.python.ptdbg_ascend.common.utils import check_path_before_create


def get_tensor_extremum(data, operator):
    if data.dtype is torch.bool:
        if operator == 'max':
            return True in data
        elif operator == 'min':
            return False not in data
    if operator == 'max':
        return torch._C._VariableFunctionsClass.max(data.float()).item()
    else:
        return torch._C._VariableFunctionsClass.min(data.float()).item()


def get_type_name(name):
    left = name.index("'")
    right = name.rindex("'")
    return name[left + 1: right]


def transfer_types(data, dtype):
    if 'int' in dtype or 'bool' in dtype:
        return int(data)
    else:
        return float(data)


def is_builtin_class(element):
    return element is None or isinstance(element, (bool, int, float, str, slice))


def analyze_device_in_kwargs(element):
    single_arg = {}
    single_arg.update({'type': 'torch.device'})
    if not isinstance(element, str):
        if hasattr(element, "index"):
            device_value = element.type + ":" + str(element.index)
        else:
            device_value = element.type
        single_arg.update({'value': device_value})
    else:
        single_arg.update({'value': element})
    return single_arg


def analyze_dtype_in_kwargs(element):
    single_arg = {}
    single_arg.update({'type': 'torch.dtype'})
    single_arg.update({'value': str(element)})
    return single_arg


class APIInfo:
    def __init__(self, api_name, save_path, is_save_data=False):
        self.api_name = api_name
        self.torch_object_key = {'device': analyze_device_in_kwargs, 'dtype': analyze_dtype_in_kwargs}
        self.rank = os.getpid()
        self.is_save_data = is_save_data
        self.save_path = save_path
        self.args_num = 0

    @staticmethod
    def get_full_save_path(save_path, dir_name, contain_step=False):
        if contain_step:
            from api_accuracy_checker.dump.dump import DumpUtil
            step_dir = "step" + str(DumpUtil.call_num - 1 if msCheckerConfig.enable_dataloader else DumpUtil.call_num)
            rank_dir = f"rank{os.getpid()}"
            return os.path.join(save_path, step_dir, dir_name, rank_dir)
        else:
            return os.path.join(save_path, dir_name)

    def analyze_element(self, element):
        if isinstance(element, (list, tuple)):
            out = []
            for item in element:
                out.append(self.analyze_element(item))
            return out

        if isinstance(element, dict):
            out_dict = {}
            for key, value in element.items():
                if key in self.torch_object_key.keys():
                    fun = self.torch_object_key[key]
                    out_dict[key] = fun(value)
                else:
                    out_dict[key] = self.analyze_element(value)
            return out_dict

        if isinstance(element, torch.Tensor):
            return self._analyze_tensor(element)

        if is_builtin_class(element):
            return self._analyze_builtin(element)

        msg = f"Type {type(element)} is unsupported at analyze_element"
        print_error_log(msg)
        raise DumpException(DumpException.INVALID_DATA_ERROR)

    def _analyze_tensor(self, arg):
        single_arg = {}
        if not self.is_save_data:
            single_arg.update({'type': 'torch.Tensor'})
            single_arg.update({'dtype': str(arg.dtype)})
            single_arg.update({'shape': arg.shape})
            single_arg.update({'Max': transfer_types(get_tensor_extremum(arg, 'max'), str(arg.dtype))})
            single_arg.update({'Min': transfer_types(get_tensor_extremum(arg, 'min'), str(arg.dtype))})
            single_arg.update({'requires_grad': arg.requires_grad})
        else:
            api_args = self.api_name + '.' + str(self.args_num)
            check_path_before_create(self.save_path)
            create_directory(self.save_path)
            file_path = os.path.join(self.save_path, f'{api_args}.pt')
            pt_path = write_pt(file_path, arg.contiguous().cpu().detach())
            self.args_num += 1
            single_arg.update({'type': 'torch.Tensor'})
            single_arg.update({'datapath': pt_path})
            single_arg.update({'requires_grad': arg.requires_grad})
        return single_arg

    def _analyze_builtin(self, arg):
        single_arg = {}
        if self.is_save_data:
            self.args_num += 1
        if isinstance(arg, slice):
            single_arg.update({'type': "slice"})
            single_arg.update({'value': [arg.start, arg.stop, arg.step]})
        else:
            single_arg.update({'type': get_type_name(str(type(arg)))})
            single_arg.update({'value': arg})
        return single_arg


class ForwardAPIInfo(APIInfo):
    def __init__(self, name, args, kwargs):
        super().__init__(name,
                         self.get_full_save_path(msCheckerConfig.dump_path, 'forward_real_data', contain_step=True),
                         is_save_data=msCheckerConfig.real_data)
        self.api_info_struct = {}
        self.stack_info_struct = {}
        self.analyze_api_input(args, kwargs)
        self.analyze_api_call_stack()

    def analyze_api_input(self, args, kwargs):
        args_info_list = self.analyze_element(args)
        kwargs_info_dict = self.analyze_element(kwargs)
        self.api_info_struct = {self.api_name: {"args": args_info_list, "kwargs": kwargs_info_dict}}

    def analyze_api_call_stack(self):
        stack_str = []
        for (_, path, line, func, code, _) in inspect.stack()[3:]:
            if not code:
                continue
            stack_line = " ".join([
                "File", ", ".join([path, " ".join(["line", str(line)]), " ".join(["in", func]),
                                   " ".join(["\n", code[0].strip()])])])
            stack_str.append(stack_line)
        self.stack_info_struct = {self.api_name: stack_str}


class BackwardAPIInfo(APIInfo):
    def __init__(self, name, grads):
        super().__init__(name,
                         self.get_full_save_path(msCheckerConfig.dump_path, 'backward_real_data', contain_step=True),
                         is_save_data=msCheckerConfig.real_data)
        self.grad_info_struct = {}
        self.analyze_api_input(grads)

    def analyze_api_input(self, grads):
        grads_info_list = self.analyze_element(grads)
        self.grad_info_struct = {self.api_name: grads_info_list}
